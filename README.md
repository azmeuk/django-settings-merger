[![build status](https://ci.gitlab.com/projects/6935/status.png?ref=master)](https://ci.gitlab.com/projects/6935?ref=master)
This little piece of code attempts to update a `settings.py` against a newer one. Here is [the original issue](http://stackoverflow.com/questions/32371347/how-to-automatically-update-django-settings-file-against-a-new-version) that made this project exist.

### The issue
We suppose that you do not version `settings.py` but a file called `settings.sample.py` instead. 
When you deploy your django project, you just copy `settings.sample.py` to `settings.py`.
Now the problem is that when you update `settings.sample.py`,
you have to manually update `settings.py` in every location you have deployed your project.
django-settings-merger attemps to automatically merge the new `settings.sample.py` with your `settings.py`:

 - Preserve manually added settings, imports and comments.
 - Insert the new settings, comments, and import, at the right place in your code.
 - If you can give the old version of your reference file (i.e. the version of `settings.sample.py` from which is built your current `settings.py`), it will replace the old default values by the new ones.

### Requirements and tests

```bash
# Install the requirements
pip install redbaron

# Tests that the script works
cd tests
source test.sh
```

### State of the development

Currently it:
  - Inserts the new settings at the right place (works for assignments and dicts).
  - Update the default values if a past sample setting file has been given.
  - Removes all the current comments and imports from the current settings.py and push all the comments and import from the reference file.
