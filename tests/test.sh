#!/bin/sh
RESULT=0

launch_test() {
    D=$1
    python ../django-settings-merger.py "${D}/settings.py" "${D}/settings.new.sample.py" --output "${D}/settings.new.py" --old-reference "${D}/settings.old.sample.py"
    cmp -s "${D}/settings.new.py" "${D}/settings.new.reference.py"
    RETURN=$?
    if [ "$RETURN" = 1 ]; then
        echo "${D} test failed"
        diff "${D}/settings.new.py" "${D}/settings.new.reference.py"
        RESULT=1
    else
        echo "${D} test succeed"
    fi
}

if [ -n "$1" ]; then
    launch_test "$1"
else
    for D in *; do
        if [ -d "${D}" ]; then
            launch_test "${D}"
        fi
    done
fi

exit "$RESULT"
