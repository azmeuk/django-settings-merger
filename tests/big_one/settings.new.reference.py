import foo, bar

####################
# Some section title
####################

FOO1 = False
FOO_OneAndAHalf = False
FOO2 = False

####################
# Some section title
####################

FOO3 = {
    "a":"b",
    "x":"y",
    "c":"d",
}
FOO4 = [
    "foobar",
    "boofar",
]
