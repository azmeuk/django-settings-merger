#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import sys
import os
from redbaron import RedBaron, AssignmentNode, DictitemNode, ImportNode, CommentNode, EndlNode

def merge_nodes(ref, out, old):
    last = None
    # Parses all the nodes in the reference file
    for ref_node in ref:
        identifiers = [
            (AssignmentNode, lambda node:node.target.value == ref_node.target.value),
            (DictitemNode, lambda node:node.key.value == ref_node.key.value),
        ]

        # Check if we can deal with the current node type
        for (node_type, identifier_func) in identifiers:
            if isinstance(ref_node, node_type):
                old_node = None

                # Finds the matching assignment in the source file
                out_node = out.find(node_type.__name__, identifier_func)

                # Insert it to the output if it does not exist yet
                if out_node is None:
                    last.insert_after(ref_node) if last else out.insert(0, ref_node)
                    out_node = out.find(node_type.__name__, identifier_func)
                    if out_node is None:
                        raise Exception("Could not append %s to the output file" % ref_node)

                # If the assignment exists, look in the reference file to see if it is a past default value.
                # And if it is, update it to the new default value.
                else:
                    if old is not None and ref_node.value.value != out_node.value.value:
                        old_node = old.find(node_type.__name__, identifier_func)
                        if old_node is not None and old_node.value.value == out_node.value.value:
                            out_node.replace(ref_node)

                last = out_node

                # Recursive call to parse values
                merge_nodes(ref_node.value.value, out_node.value.value, old_node.value.value if old_node else None)

    return out

def remove_nodes(node_types, out):
    remove_list = []
    for node in out:
        if node.__class__ in node_types:
            remove_list.append(node)

    for node in remove_list:
        out.remove(node)
    return out

def push_nodes(node_types, ref, out):
    last_index = 0
    for ref_node in ref:
        if isinstance(ref_node, AssignmentNode):
            out_node = out.find("AssignmentNode", lambda node:node.target.value == ref_node.target.value)
            if out_node is None:
                raise Exception("The node %s should have been found" % ref_node)
            else:
                last_index = out_node.index_on_parent+1
        
        elif ref_node.__class__ in node_types:
            out.insert(last_index, ref_node)
            last_index += 1

    return out

def merge_py_settings_files(source, reference, output=None, old_ref=None):
    if not os.path.exists(source):
        print "Source file %s does not exist" % source
        return False

    if not os.path.exists(reference):
        print "Reference file %s does not exist" % reference
        return False

    with open(source, 'r') as code:
        redsrc = RedBaron(code.read())

    with open(reference, 'r') as code:
        redref = RedBaron(code.read())

    redold = None
    if old_ref is not None and os.path.exists(old_ref):
        with open(old_ref, 'r') as code: 
            redold = RedBaron(code.read())

    redout = redsrc

    # Remove all comments and imports
    redout = remove_nodes([CommentNode, ImportNode, EndlNode], redout)

    # Copy the new values
    redout = merge_nodes(redref, redout, redold)

    # Push the new comments and imports
    redout = push_nodes([CommentNode, ImportNode, EndlNode], redref, redout)

    with open(output if output is not None else source, 'w') as code:
        code.write(redout.dumps())

if (__name__ == '__main__'):
    parser = argparse.ArgumentParser(description='Takes a <source>, <new_reference> and an <old_rerefence> python settings files. Merge the new settings and the new default values of <new_reference> mixed with <source> values in the <output> file.')
    parser.add_argument('source', metavar='source', type=str, help='The settings.py to be updated')
    parser.add_argument('new_reference', metavar='new_reference', type=str, help='The new settings.py sample file')
    parser.add_argument('--output', metavar='output', type=str, help='The output file')
    parser.add_argument('--old-reference', metavar='old_reference', type=str, help='The old settings.py sample file')

    args = parser.parse_args()
    sys.exit(not merge_py_settings_files(args.source, args.new_reference, args.output, args.old_reference))
